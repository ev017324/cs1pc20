#include <iostream>
using namespace std;

int main() {
    int rows, columns;
    cout &#8203;`oaicite:{"index":0, :"<< \"Enter the number of rows: \";\n    cin >>"}`&#8203; rows;
    cout &#8203;`oaicite:{"index":1,"<< \"Enter the number of columns: \";\n    cin >>"}`&#8203; columns;

    int arr1[rows][columns], arr2[rows][columns], arr3[rows][columns];

    cout << "Enter elements of first matrix:" << endl;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            cin >> arr1[i][j];
        }
    }

    cout << "Enter elements of second matrix:" << endl;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            cin >> arr2[i][j];
        }
    }

    // Perform element-wise addition of both matrices
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            arr3[i][j] = arr1[i][j] + arr2[i][j];
        }
    }

    cout << "Resultant matrix:" << endl;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            cout << arr3[i][j] << " ";
        }
        cout << endl;
    }

    return 0;
}



