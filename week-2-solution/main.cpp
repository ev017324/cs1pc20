#include "floatList.h"

int main(void) {
    floatList list;
    list.appendNode(2.5);
    list.appendNode(7.9);
    list.appendNode(12.6);
    list.appendNode(5.8);
    list.displayList();
    list.deleteNode(7.9);
    list.displayList();
    list.deleteNode(2.5);
    list.appendNode(6.2);
    list.displayList();
    list.deleteNode(6.2);
    list.displayList();
    return 0;
}
