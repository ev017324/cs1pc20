#pragma once
class floatList
{
private:
    // Declare a structure for the list
    struct ListNode {
        float value;
        struct ListNode* next;
    };
    ListNode* head; // List head pointer
public:
    floatList(void) { // Constructor
        head = nullptr;
    }
    ~floatList(void) { }; // Destructor
    void appendNode(float);
    void displayList(void);
    void deleteNode(float);
};
