#include "floatList.h"
#include <iostream>

void floatList::appendNode(float num) {
    ListNode* newNode = new ListNode;
    newNode->value = num;
    newNode->next = nullptr;

    if (head == nullptr) {
        head = newNode;
    }
    else {
        ListNode* nodePtr = head;
        while (nodePtr->next != nullptr) {
            nodePtr = nodePtr->next;
        }
        nodePtr->next = newNode;
    }
}

void floatList::displayList() {
    ListNode* nodePtr = head;
    while (nodePtr != nullptr) {
        std::cout << nodePtr->value << std::endl;
        nodePtr = nodePtr->next;
    }
    std::cout << std::endl;
}

void floatList::deleteNode(float num) {
    ListNode* nodePtr = head;
    ListNode* prevNode = nullptr;
    while (nodePtr != nullptr && nodePtr->value != num) {
        prevNode = nodePtr;
        nodePtr = nodePtr->next;
    }
    if (nodePtr != nullptr) {
        if (prevNode == nullptr) {
            head = head->next;
        }
        else {
            prevNode->next = nodePtr->next;
        }
        std::cout << num << " is deleted" << std::endl;
        delete nodePtr;
    }
}
