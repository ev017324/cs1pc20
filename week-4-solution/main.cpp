#include <iostream>
#include "floatList.h"

int main(void)
{
    FloatList list1;
    list1.appendNode(1);
    list1.appendNode(5);
    list1.appendNode(9);
    list1.appendNode(14);
    list1.appendNode(15);
    list1.appendNode(21);
    std::cout << "Printing list 1: " << std::endl;
    list1.displayList();

    FloatList list2;
    list2.appendNode(9);
    list2.appendNode(10);
    list2.appendNode(14);
    list2.appendNode(19);
    list2.appendNode(29);
    list2.appendNode(37);
    std::cout << "Printing list 2: " << std::endl;
    list2.displayList();

    FloatList::ListNode* list3 = FloatList::mergeLists(list1.head, list2.head);
    std::cout << "Printing list 3: " << std::endl;
    FloatList::removeDuplicatesInMergedLists(list3);
    list1.displayList();

    std::cout << "Printing list 4: " << std::endl;
    list1.displayList();

    return 0;
}

