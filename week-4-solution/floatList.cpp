#include "floatList.h"
#include <iostream>

void FloatList::appendNode(float num) {
    ListNode* newNode = new ListNode;
    newNode->value = num;
    newNode->next = nullptr;
    if (head == nullptr) {
        head = newNode;
    }
    else {
        ListNode* nodePtr = head;
        while (nodePtr->next != nullptr) {
            nodePtr = nodePtr->next;
        }
        nodePtr->next = newNode;
    }
}

void FloatList::displayList() {
    ListNode* nodePtr = head;
    while (nodePtr != nullptr) {
        std::cout << nodePtr->value << std::endl;
        nodePtr = nodePtr->next;
    }
    std::cout << std::endl;
}

void FloatList::deleteNode(float num) {
    ListNode* nodePtr = head;
    ListNode* prevNode = nullptr;
    while (nodePtr != nullptr && nodePtr->value != num) {
        prevNode = nodePtr;
        nodePtr = nodePtr->next;
    }
    if (nodePtr != nullptr) {
        if (prevNode == nullptr) {
            head = head->next;
        }
        else {
            prevNode->next = nodePtr->next;
        }
        std::cout << num << " is deleted" << std::endl;
        delete nodePtr;
    }
}

FloatList::ListNode* FloatList::mergeLists(FloatList::ListNode* l1, FloatList::ListNode* l2)
{
    FloatList::ListNode* mergedList = nullptr;
    FloatList::ListNode** tail = &mergedList;

    while (true) {
        if (l1 == nullptr) {
            *tail = l2;
            break;
        }
        else if (l2 == nullptr) {
            *tail = l1;
            break;
        }

        if (l1->value <= l2->value) {
            *tail = l1;
            l1 = l1->next;
        }
        else {
            *tail = l2;
            l2 = l2->next;
        }
        tail = &((*tail)->next);
    }

    return mergedList;
}

FloatList::ListNode* FloatList::removeDuplicatesInMergedLists(FloatList::ListNode* l3)
{
    if (l3 == nullptr) {
        return nullptr;
    }

    FloatList::ListNode* currentNode = l3;
    while (currentNode->next != nullptr) {
        if (currentNode->value == currentNode->next->value) {
            FloatList::ListNode* nodeToDelete = currentNode->next;
            currentNode->next = currentNode->next->next;
            delete nodeToDelete;
        }
        else {
            currentNode = currentNode->next;
        }
    }

    return l3;
}

