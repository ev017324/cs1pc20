#pragma once

class FloatList
{
public:
    // Declare a structure for the list
    struct ListNode {
        float value;
        struct ListNode* next;
    };
    ListNode* head; // List head pointer
    FloatList(void) { // Constructor
        head = nullptr;
    }
    ~FloatList(void) { }; // Destructor
    void appendNode(float);
    void displayList(void);
    ListNode* mergeLists(ListNode*, ListNode*);
    ListNode* removeDuplicatesInMergedLists(ListNode*);
};

