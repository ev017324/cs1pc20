#ifndef FLOATLIST_H
#define FLOATLIST_H

class FloatList {
public:
    FloatList();
    void append(float value);
    void print();
    void printReverse();
    void concatenate(FloatList* list);
private:
    struct Node {
        float data;
        Node* next;
    };
    Node* head;
    Node* tail;
    int size;
};

#endif // FLOATLIST_H

