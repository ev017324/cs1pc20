#include "floatList.h"
#include <iostream>

FloatList::FloatList() {
    head = nullptr;
    tail = nullptr;
    size = 0;
}

void FloatList::append(float value) {
    Node* newNode = new Node{value, nullptr};
    if (head == nullptr) {
        head = newNode;
        tail = newNode;
    } else {
        tail->next = newNode;
        tail = newNode;
    }
    size++;
}

void FloatList::print() {
    Node* current = head;
    while (current != nullptr) {
        std::cout << current->data << std::endl;
        current = current->next;
    }
}

void FloatList::printReverse() {
    Node* current = tail;
    while (current != nullptr) {
        std::cout << current->data << std::endl;
        current = current->prev;
    }
}

void FloatList::concatenate(FloatList* list) {
    tail->next = list->head;
    tail = list->tail;
    size += list->size;
}

