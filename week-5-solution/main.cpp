
#include "floatList.h"

int main() {
    FloatList list1;
    list1.append(1);
    list1.append(5);
    list1.append(9);
    list1.append(14);
    list1.append(15);
    list1.append(21);

    FloatList list2;
    list2.append(9);
    list2.append(10);
    list2.append(14);
    list2.append(19);
    list2.append(29);
    list2.append(37);

    std::cout << "Printing list 1:" << std::endl;
    list1.print();

    std::cout << "Printing list 2:" << std::endl;
    list2.print();

    list1.concatenate(&list2);

    std::cout << "Printing list 3:" << std::endl;
    list1.print();

    std::cout << "Printing list 4:" << std::endl;
    list1.print();

    std::cout << "Printing list 1 in reverse order:" << std::endl;
    list1.printReverse();

    std::cout << "Printing list 1 in original order:" << std::endl;
    list1.print();

    return 0;
}
